/**
 * Created by joy on 2/24/17.
 */

import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.data.Stat;

public class ZKExists {
    private static ZooKeeper zk;
    private static ZooKeeperConnection conn;

    // Method to check existence of znode and its status, if znode is available.
    public static Stat znode_exists(String path) throws
            KeeperException, InterruptedException {

        return zk.exists(path, true);
    }

    public static boolean getZKStatus(String zkNnodePath) {
        String path = zkNnodePath; // Assign znode to the specified path

        try {
            conn = new ZooKeeperConnection();
            zk = conn.connect("192.168.100.124");
            Stat stat = znode_exists(path); // Stat checks the path of the znode
            boolean result = false ;
            if (stat != null) {
                System.out.println("Node exists and the node version is " +  stat.getVersion());
                result = true ;
            } else {
                System.out.println("Node does not exists");
                result = false;
            }
            return result;
        } catch (Exception e) {
            System.out.println(e.getMessage()); // Catches error messages
        }
        return false;
    }
}
