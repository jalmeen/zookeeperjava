/**
 * Created by joy on 2/24/17.
 */
import org.apache.log4j.BasicConfigurator;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;

public class ZooKeeperMain {
    // create static instance for zookeeper class.
    private static ZooKeeper zk;

    // create static instance for ZooKeeperConnection class.
    private static ZooKeeperConnection conn;

    // Method to create znode in zookeeper ensemble
    public static void create(String path, byte[] data) throws
            KeeperException,InterruptedException {
        zk.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE,
                CreateMode.PERSISTENT);
    }

    public static void main(String[] args) {
        BasicConfigurator.configure();

        // znode path
        String path = "/MyFirstZnode"; // Assign path to znode

        // data in byte array
        byte[] data = " My first zookeeper app".getBytes(); // Declare data
        if (ZKExists.getZKStatus(path) != false)
        {
            try {
                conn = new ZooKeeperConnection();
                zk = conn.connect("192.168.100.124");
                create(path, data); // Create the data to the specified path
                conn.close();
            } catch (Exception e) {
                System.out.println(e.getMessage()); //Catch error message
            }
        }
    }
}
